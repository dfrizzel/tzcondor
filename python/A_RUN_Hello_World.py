from MotherCondor import MotherCondor
from glob import glob
from condor_utils import *
from pprint import pprint
import os
import pdb
#THESE ARE ESSENTIALLY JOBOPTIONS
os.system('cls' if os.name == 'nt' else 'clear')
Regions = ["SignalRegion","ControlRegion"]
DSIDs = ["Signal","Background"]
outputDir = "./"
print("I am going to use this path to tZq analysis:")
print(tZqAnalysis_path)
print("---------------------------------------")
MC = MotherCondor()
pdb.set_trace()
for region in Regions:
    for DSID in DSIDs:
        name = "HelloWorld_" + region + "_" + DSID
        content = """#! /bin/sh
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
. /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
localSetupROOT 6.12.06-x86_64-slc6-gcc62-opt
lsetup "lcgenv -p LCG_87"
echo 'Hello job named """+name +"""' > """+name+""".txt"""  
        MC.make_job(name, content)	
        MC.make_cmd( name )
	print("Launching " + name)
        MC.LetFly(name )
