from condor_utils import tZqAnalysis_path

cmd_template = """universe        = vanilla
output          = """+tZqAnalysis_path+"""tzcondor/OUT/%s.out
error           = """+tZqAnalysis_path+"""tzcondor/ERR/%s.err
log             = """+tZqAnalysis_path+"""tzcondor/LOG/%s.log
notification    = never
executable = """+tZqAnalysis_path+"""tzcondor/JOBs/%s.sh
queue
"""
job_template = """#!/bin/bash
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
. /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
localSetupROOT 6.12.06-x86_64-slc6-gcc62-opt
echo "MAKE SURE YOU HAVE COMPILED NTR WITH LATEST ROOT"
%s
"""
