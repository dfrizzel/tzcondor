from MotherCondor import MotherCondor
from glob import glob
from condor_utils import *
from templates import *
from pprint import pprint
import os
#THESE ARE ESSENTIALLY JOBOPTIONS
DSIDs = glob(tZqAnalysis_path + "NtupleReader/samples/*.def")
RUN_SYSTEMATICS = True
Configs = ["SR-3j1b.yaml","SR-2j1b.yaml"]
trees = ["nominal"]
# END JOB OPTIONS
 
os.system('cls' if os.name == 'nt' else 'clear')
print("I am going to use this path to tZq analysis:")
print(tZqAnalysis_path)
print("---------------------------------------")


print("---------------------------------------")
print("I am going to use the configs: " + str(Configs) )
print("---------------------------------------")

print("---------------------------------------")
print("I am going to run over these DSIDs...")
print("---------------------------------------")
pprint(DSIDs)

#Reformat DSIDs list
for x in range(0,len(DSIDs)):
    DSIDs[x] = DSIDs[x][ DSIDs[x].rfind('/') + 1 :-4]



if RUN_SYSTEMATICS:
    f = open(tZqAnalysis_path+"Resources/config/SystematicTrees_v25.txt","r")
    for line in f:
      trees.append(line.rstrip())
    f.close()


print("---------------------------------------")
print("---------------------------------------")
command = "./Driver %s -1 ConfigFile=%s TreeName=%s Outputname=%s Outputdir=condor/%s" 
MC = MotherCondor()
for Config in Configs:	
    for tree in trees:
        for dsid in DSIDs:

            if dsid[0] == "d" and tree not in ["nominal","nominal_Loose"]:#don't run systematics on data
                continue
            name = dsid+"_"+tree+"_"+Config.split(".")[0]	
            if RUN_SYSTEMATICS and "SR" in Config:
                MC.make_job(name, job_template % ( command % (dsid, tZqAnalysis_path + "NtupleReader/ConfigFiles/"+Config ,tree,tree, (Config.split('.')[0])+"--" + tree)  ) )
            else:
                MC.make_job(name, job_template % ( "./Driver %s -1 ConfigFile=%s TreeName=%s Outputname=%s " % (dsid, tZqAnalysis_path + "NtupleReader/ConfigFiles/"+Config ,tree,tree) ) )	
            MC.make_cmd( name )
            if not os.path.isfile( tZqAnalysis_path + "NtupleReader/condor/" + Config.split('.')[0] + "--" + tree +"/summary_"+tree +".MC."+dsid+".root"):
                print("Launching file :  "  + Config.split('.')[0] + "--" + tree + "/summary_"+tree +".MC."+dsid+".root")
                MC.LetFly(name )
            else:
                print("Skipping file :  " + Config.split('.')[0] + "--" + tree + "/summary_"+tree +".MC."+dsid+".root")
