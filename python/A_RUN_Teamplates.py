from MotherCondor import MotherCondor
from glob import glob
from condor_utils import *
from pprint import pprint
import os
#THESE ARE ESSENTIALLY JOBOPTIONS
trees = ["nominal"]
Regions = ["SR-2j1b","SR-3j1b"]
# END JOB OPTIONS 
SYSTEMATICS = True


if SYSTEMATICS:
    f = open(tZqAnalysis_path+"Resources/config/SystematicTrees_v25.txt","r")
    for line in f:
      trees.append(line.rstrip())
    f.close()


os.system('cls' if os.name == 'nt' else 'clear')
print("I am going to use this path to tZq analysis:")
print(tZqAnalysis_path)
print("---------------------------------------")
print("I am going to use these trees")
print(trees)

print("---------------------------------------")
print("---------------------------------------")

MC = MotherCondor()
for region in Regions:
    for tree in trees:
        name = "ExpertNeuroBayes_" + region + "_" + tree



        if "2j" in region and tree == "nominal":
            content = """#! /bin/sh
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
. /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
localSetupROOT 6.12.06-x86_64-slc6-gcc62-opt
lsetup "lcgenv -p LCG_87"
Expert=/cvmfs/sft.cern.ch/lcg/releases/LCG_87/neurobayes/3.7.0/x86_64-slc6-gcc49-opt
export NEUROBAYES=$Expert
export NEUROBAYESLIB=$NEUROBAYES/lib
export LD_LIBRARY_PATH=$NEUROBAYESLIB:$LD_LIBRARY_PATH
export PATH=$NEUROBAYES/external:$PATH
LHAPDF=/cvmfs/sft.cern.ch/lcg/external/MCGenerators_lcgcmt67c/lhapdf/6.1.5/x86_64-slc6-gcc48-opt
export LHAPDFLIB=$LHAPDF/lib
export LD_LIBRARY_PATH=$LHAPDFLIB:$LD_LIBRARY_PATH 
cd """+Expert_path+"""

## The default options can be overwriten from the ConfigFile or passing them from the run commnad. The run command has higher priorty! 

PathToInputMCFiles="""+tZqAnalysis_path+"""/NtupleReader/condor/
NNExpertFile_fold0=../TeacherNeuroBayes/OutPut/triLepton_2j_fold0_expert.nb
NNExpertFile_fold1=../TeacherNeuroBayes/OutPut/triLepton_2j_fold1_expert.nb
NNExpertFile_fold2=../TeacherNeuroBayes/OutPut/triLepton_2j_fold2_expert.nb
NNExpertFile_fold3=../TeacherNeuroBayes/OutPut/triLepton_2j_fold3_expert.nb
NNExpertFile_fold4=../TeacherNeuroBayes/OutPut/triLepton_2j_fold4_expert.nb

InputVariable=../TeacherNeuroBayes/ConfigFiles/2jet1bVariables.txt
MCSamplesID=ConfigFiles/SamplesID.txt

Option="WriteDataTemplate=1 "
Option+="ProcessSystematicWeights=1 "
Option+="PathToInputMCFiles=$PathToInputMCFiles/"""+region+"""--"""+tree+"""/ "
Option+="PathToInputDataFiles=$PathToInputMCFiles/"""+region+"""--"""+tree+"""/ "
Option+="NNExpertFile_fold0=$NNExpertFile_fold0 "
Option+="NNExpertFile_fold1=$NNExpertFile_fold1 "
Option+="NNExpertFile_fold2=$NNExpertFile_fold2 "
Option+="NNExpertFile_fold3=$NNExpertFile_fold3 "
Option+="NNExpertFile_fold4=$NNExpertFile_fold4 "
Option+="InputVariableNames=$InputVariable "
Option+="MCSamplesID=$MCSamplesID "
Option+="OutputPath=OutPut/"""+region+""" "

echo $Option
#run nominal, no data file because it is blind analysis  
./ExpertNeuroBayes ConfigFiles/ConfigFile.txt $Option
        """

        if "2j" in region and tree != "nominal":
            content = """#! /bin/sh
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
. /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
localSetupROOT 6.12.06-x86_64-slc6-gcc62-opt
lsetup "lcgenv -p LCG_87"
Expert=/cvmfs/sft.cern.ch/lcg/releases/LCG_87/neurobayes/3.7.0/x86_64-slc6-gcc49-opt
export NEUROBAYES=$Expert
export NEUROBAYESLIB=$NEUROBAYES/lib
export LD_LIBRARY_PATH=$NEUROBAYESLIB:$LD_LIBRARY_PATH
export PATH=$NEUROBAYES/external:$PATH
LHAPDF=/cvmfs/sft.cern.ch/lcg/external/MCGenerators_lcgcmt67c/lhapdf/6.1.5/x86_64-slc6-gcc48-opt
export LHAPDFLIB=$LHAPDF/lib
export LD_LIBRARY_PATH=$LHAPDFLIB:$LD_LIBRARY_PATH 
cd """+Expert_path+"""

## The default options can be overwriten from the ConfigFile or passing them from the run commnad. The run command has higher priorty! 

PathToInputMCFiles="""+tZqAnalysis_path+"""/NtupleReader/condor/
NNExpertFile_fold0=../TeacherNeuroBayes/OutPut/triLepton_2j_fold0_expert.nb
NNExpertFile_fold1=../TeacherNeuroBayes/OutPut/triLepton_2j_fold1_expert.nb
NNExpertFile_fold2=../TeacherNeuroBayes/OutPut/triLepton_2j_fold2_expert.nb
NNExpertFile_fold3=../TeacherNeuroBayes/OutPut/triLepton_2j_fold3_expert.nb
NNExpertFile_fold4=../TeacherNeuroBayes/OutPut/triLepton_2j_fold4_expert.nb

InputVariable=../TeacherNeuroBayes/ConfigFiles/2jet1bVariables.txt

MCSamplesID=ConfigFiles/SamplesID_noFakes.txt
Option="WriteDataTemplate=0 "
Option+="NNExpertFile_fold0=$NNExpertFile_fold0 "
Option+="NNExpertFile_fold1=$NNExpertFile_fold1 "
Option+="NNExpertFile_fold2=$NNExpertFile_fold2 "
Option+="NNExpertFile_fold3=$NNExpertFile_fold3 "
Option+="NNExpertFile_fold4=$NNExpertFile_fold4 "
Option+="InputVariableNames=$InputVariable "
Option+="MCSamplesID=$MCSamplesID "
Option+="OutputPath=OutPut/"""+region+""" "

Path=$PathToInputMCFiles/"""+region+"""--"""+tree+"""/
./ExpertNeuroBayes ConfigFiles/ConfigFile.txt $Option PathToInputMCFiles=$Path treename="""+tree


        if "3j" in region and tree == "nominal":
            content = """#! /bin/sh
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
. /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
localSetupROOT 6.12.06-x86_64-slc6-gcc62-opt
lsetup "lcgenv -p LCG_87"
Expert=/cvmfs/sft.cern.ch/lcg/releases/LCG_87/neurobayes/3.7.0/x86_64-slc6-gcc49-opt
export NEUROBAYES=$Expert
export NEUROBAYESLIB=$NEUROBAYES/lib
export LD_LIBRARY_PATH=$NEUROBAYESLIB:$LD_LIBRARY_PATH
export PATH=$NEUROBAYES/external:$PATH
LHAPDF=/cvmfs/sft.cern.ch/lcg/external/MCGenerators_lcgcmt67c/lhapdf/6.1.5/x86_64-slc6-gcc48-opt
export LHAPDFLIB=$LHAPDF/lib
export LD_LIBRARY_PATH=$LHAPDFLIB:$LD_LIBRARY_PATH 
cd """+Expert_path+"""

## The default options can be overwriten from the ConfigFile or passing them from the run commnad. The run command has higher priorty! 

PathToInputMCFiles="""+tZqAnalysis_path+"""/NtupleReader/condor/
NNExpertFile_fold0=../TeacherNeuroBayes/OutPut/triLepton_3j_fold0_expert.nb
NNExpertFile_fold1=../TeacherNeuroBayes/OutPut/triLepton_3j_fold1_expert.nb
NNExpertFile_fold2=../TeacherNeuroBayes/OutPut/triLepton_3j_fold2_expert.nb
NNExpertFile_fold3=../TeacherNeuroBayes/OutPut/triLepton_3j_fold3_expert.nb
NNExpertFile_fold4=../TeacherNeuroBayes/OutPut/triLepton_3j_fold4_expert.nb

InputVariable=../TeacherNeuroBayes/ConfigFiles/3jet1bVariables.txt
MCSamplesID=ConfigFiles/SamplesID.txt

Option="WriteDataTemplate=1 "
Option+="ProcessSystematicWeights=1 "
Option+="PathToInputMCFiles=$PathToInputMCFiles/"""+region+"""--"""+tree+"""/ "
Option+="PathToInputDataFiles=$PathToInputMCFiles/"""+region+"""--"""+tree+"""/ "
Option+="NNExpertFile_fold0=$NNExpertFile_fold0 "
Option+="NNExpertFile_fold1=$NNExpertFile_fold1 "
Option+="NNExpertFile_fold2=$NNExpertFile_fold2 "
Option+="NNExpertFile_fold3=$NNExpertFile_fold3 "
Option+="NNExpertFile_fold4=$NNExpertFile_fold4 "
Option+="InputVariableNames=$InputVariable "
Option+="MCSamplesID=$MCSamplesID "
Option+="OutputPath=OutPut/"""+region+""" "

echo $Option
#run nominal, no data file because it is blind analysis  
./ExpertNeuroBayes ConfigFiles/ConfigFile.txt $Option
        """
        if "3j" in region and tree != "nominal":
            content = """#! /bin/sh
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
. /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
localSetupROOT 6.12.06-x86_64-slc6-gcc62-opt
lsetup "lcgenv -p LCG_87"
Expert=/cvmfs/sft.cern.ch/lcg/releases/LCG_87/neurobayes/3.7.0/x86_64-slc6-gcc49-opt
export NEUROBAYES=$Expert
export NEUROBAYESLIB=$NEUROBAYES/lib
export LD_LIBRARY_PATH=$NEUROBAYESLIB:$LD_LIBRARY_PATH
export PATH=$NEUROBAYES/external:$PATH
LHAPDF=/cvmfs/sft.cern.ch/lcg/external/MCGenerators_lcgcmt67c/lhapdf/6.1.5/x86_64-slc6-gcc48-opt
export LHAPDFLIB=$LHAPDF/lib
export LD_LIBRARY_PATH=$LHAPDFLIB:$LD_LIBRARY_PATH 
cd """+Expert_path+"""

## The default options can be overwriten from the ConfigFile or passing them from the run commnad. The run command has higher priorty! 

PathToInputMCFiles="""+tZqAnalysis_path+"""/NtupleReader/condor/
NNExpertFile_fold0=../TeacherNeuroBayes/OutPut/triLepton_3j_fold0_expert.nb
NNExpertFile_fold1=../TeacherNeuroBayes/OutPut/triLepton_3j_fold1_expert.nb
NNExpertFile_fold2=../TeacherNeuroBayes/OutPut/triLepton_3j_fold2_expert.nb
NNExpertFile_fold3=../TeacherNeuroBayes/OutPut/triLepton_3j_fold3_expert.nb
NNExpertFile_fold4=../TeacherNeuroBayes/OutPut/triLepton_3j_fold4_expert.nb

InputVariable=../TeacherNeuroBayes/ConfigFiles/3jet1bVariables.txt

MCSamplesID=ConfigFiles/SamplesID_noFakes.txt
Option="WriteDataTemplate=0 "
Option+="NNExpertFile_fold0=$NNExpertFile_fold0 "
Option+="NNExpertFile_fold1=$NNExpertFile_fold1 "
Option+="NNExpertFile_fold2=$NNExpertFile_fold2 "
Option+="NNExpertFile_fold3=$NNExpertFile_fold3 "
Option+="NNExpertFile_fold4=$NNExpertFile_fold4 "
Option+="InputVariableNames=$InputVariable "
Option+="MCSamplesID=$MCSamplesID "
Option+="OutputPath=OutPut/"""+region+""" "

Path=$PathToInputMCFiles/"""+region+"""--"""+tree+"""/
./ExpertNeuroBayes ConfigFiles/ConfigFile.txt $Option PathToInputMCFiles=$Path treename="""+tree


        MC.make_job(name, content)	
        MC.make_cmd( name )
        MC.LetFly(name )
