from template import cmd_template
import subprocess
from condor_utils import tZqAnalysis_path
import time


def PopenWait(command):
    try:
        comm = None
        proc = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        comm = proc.communicate() 
        return comm
    except Exception as e:
        return e


class MotherCondor(object):

    def __init__(self,treelist=[],DSIDs=[]):
        self.treelist = treelist
        self.DSIDs = DSIDs
        self.tZqAnalysis_path =  tZqAnalysis_path

    def make_job(self,filename,content):
        out = open(self.tZqAnalysis_path+"/tzcondor/JOBs/"+filename+".sh",'w')
        out.write(content)
        out.close()
        PopenWait("chmod +x " + self.tZqAnalysis_path + 'tzcondor/JOBs/'+filename+".sh")
        return 0
    
    def make_cmd(self,name):
        out = open(self.tZqAnalysis_path + '/tzcondor/CMDs/' + name + ".cmd",'w')
        out.write(cmd_template % ((name,)*4))
        out.close()
        return 0

    def LetFly(self,name):
        PopenWait("condor_submit " + self.tZqAnalysis_path + "tzcondor/CMDs/%s" % name + ".cmd")
	time.sleep(0.25)


